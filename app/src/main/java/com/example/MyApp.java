package com.example;

import android.app.Application;
import android.content.Context;

public class MyApp extends Application {

    private static MyApp instance;

    @Override
    public void onCreate() {
        super.onCreate();

        instance=this;
        initializeInjector();

        // Setup handler for uncaught exceptions.
        Thread.setDefaultUncaughtExceptionHandler(this::handleUncaughtException);
    }

    private void initializeInjector() {


    }

    public static MyApp getAppComponent() {
        return instance;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    public void handleUncaughtException(Thread thread, Throwable e) {
        Thread.UncaughtExceptionHandler uch = Thread.getDefaultUncaughtExceptionHandler();
        e.printStackTrace(); // not all Android versions will print the stack trace automatically

        System.out.println("UncaughtException is handled!");

        System.exit(-1);
    }
}