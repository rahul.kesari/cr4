package com.example.utils;

/**
 * Created by rahul on 1/5/2018.
 */

public interface Constants {

    String STATUS_NEW = "new";
    String STATUS_ORDERED = "ordered";
    String STATUS_NA = "na";
    String STATUS_DROPPED = "dropped";
    String FILTER_SORTY_BY = "sort_by";
    String FILTER_FROM_DATE = "from_date";
    String FILTER_TO_DATE = "to_date";
    String FILTER_INCLUDE_ALL_LINES = "include_all_lines";

    String FILTER_STATUS_NEW = "filter_status_new";
    String FILTER_STATUS_ORDERED = "filter_status_ordered";
    String FILTER_STATUS_NA = "filter_status_na";
    String FILTER_STATUS_DROPPED = "filter_status_dropped";
    String ASCENDING = "asc";
    String DESCENDING = "desc";
    String FILTER_REQUEST_DATA = "filter_request_data";
}
