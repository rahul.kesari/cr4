package com.example.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTimerHelper {

    private static DateTimerHelper instance;

    public static DateTimerHelper getInstance() {
        if (instance == null) {
            instance = new DateTimerHelper();
        }
        return instance;
    }

    public String getCurrentDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }
}
