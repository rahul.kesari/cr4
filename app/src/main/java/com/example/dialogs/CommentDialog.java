package com.example.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.example.R;
import com.example.utils.customviews.CustomEditText;
import com.example.utils.customviews.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by innotical on 25/5/17.
 */

public class CommentDialog extends Dialog {

    @BindView(R.id.commentET)
    CustomEditText commentET;
    @BindView(R.id.saveBtn)
    CustomTextView saveBtn;
    @BindView(R.id.cancelBtn)
    CustomTextView cancelBtn;
    private Context context;

    private OkListerner okListerner;

    public CommentDialog(Context context) {
        super(context, R.style.mydialog);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_comment);
        ButterKnife.bind(this);


    }


    @OnClick({R.id.saveBtn, R.id.cancelBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.saveBtn:
                if (commentET.getText().toString().trim().length() != 0) {
                    if (okListerner != null) {
                        okListerner.onOkClicked(commentET.getText().toString().trim());
                        dismiss();
                    }
                } else {
                    commentET.setError("Please Enter Comment");
                }
                break;
            case R.id.cancelBtn:
                dismiss();
                break;
        }

    }


    public interface OkListerner {
        public void onOkClicked(String comment);
    }

    public void setonOkListener(OkListerner listener) {
        okListerner = listener;
    }

}