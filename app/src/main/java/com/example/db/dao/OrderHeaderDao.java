package com.example.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.db.entity.OrderHeader;
import com.example.db.entity.OrderLine;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface OrderHeaderDao {

    @Query("SELECT * FROM order_header")
    List<OrderHeader> getAllOrderHeaders();

    /*@Query("SELECT * FROM user where first_name LIKE  :firstName AND last_name LIKE :lastName")
    User findByName(String firstName, String lastName);
*/
    @Query("SELECT COUNT(*) from order_header")
    int countRecords();

    @Insert
    void insertAll(OrderHeader... orderHeaders);

    @Delete
    void delete(OrderHeader orderHeader);

   /* @Query("UPDATE Tour SET endAddress = :end_address WHERE id = :tid")
    int updateTour(long tid, String end_address);*/


    @Query("UPDATE order_header SET status = :status, comments=:comments , action_date=:updatedTimeStamp WHERE order_id = :orderId")
    int updateOrderHeaderStatus(String status, String comments, String updatedTimeStamp, String orderId);

//    @Update
//    int updateOrderHeaderStatus(OrderHeader orderHeader);
}
