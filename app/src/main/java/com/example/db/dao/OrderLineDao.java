package com.example.db.dao;

import android.arch.persistence.db.SupportSQLiteQuery;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.RawQuery;
import android.arch.persistence.room.Update;

import com.example.db.entity.OrderHeader;
import com.example.db.entity.OrderLine;

import java.util.List;

@Dao
public interface OrderLineDao {

    @Delete
    void delete(OrderLine orderLine);

    @Query("SELECT COUNT(*) FROM order_line ")
    int getNumberOfRows();

    @Insert
    void insertAll(OrderLine... orderLines);

    @Query("SELECT * FROM order_line ORDER BY create_date DESC")
    List<OrderLine> getAllOrderLines();
/*

     @Query("UPDATE order_header SET action_date = :end_address WHERE id = :tid")
    int updateTour(long tid, String end_address);
*/

    @Update
    int updateOrderLineStatus(OrderLine tour);

    @RawQuery
    List<OrderLine> getAllOrderLines(SupportSQLiteQuery query);
}

