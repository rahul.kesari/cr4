package com.example.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

//primaryKeys = {"firstName", "lastName"}
@Entity(tableName = "order_header")
public class OrderHeader {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "order_id")
    private String orderId;

    @ColumnInfo(name = "order_type")
    private String orderType;

    @ColumnInfo(name = "ordering_store")
    private String orderingStore;

    @ColumnInfo(name = "ordered_by")
    private String orderedBy;

    @ColumnInfo(name = "status")
    private String status;

    @ColumnInfo(name = "currency")
    private String currency;

    @ColumnInfo(name = "create_date")
    private String createDate;

    @ColumnInfo(name = "approved_by")
    private String approvedBy;

    @ColumnInfo(name = "declined_by")
    private String declinedBy;

    @ColumnInfo(name = "comments")
    private String comments;

    @ColumnInfo(name = "action_date")
    private String actionDate;

    @ColumnInfo(name = "total_amount")
    private int totalAmount;

    @ColumnInfo(name = "party_id")
    private String partyId;

    @ColumnInfo(name = "reference_id1")
    private String referenceId1;


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public void setActionDate(String actionDate) {
        this.actionDate = actionDate;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setDeclinedBy(String declinedBy) {
        this.declinedBy = declinedBy;
    }

    public void setOrderedBy(String orderedBy) {
        this.orderedBy = orderedBy;
    }

    public void setOrderingStore(String orderingStore) {
        this.orderingStore = orderingStore;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

    public void setReferenceId1(String referenceId1) {
        this.referenceId1 = referenceId1;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    public int getId() {
        return id;
    }

    public String getOrderType() {
        return orderType;
    }

    public String getStatus() {
        return status;
    }

    public String getOrderingStore() {
        return orderingStore;
    }

    public String getCreateDate() {
        return createDate;
    }

    public String getComments() {
        return comments;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public String getActionDate() {
        return actionDate;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public String getCurrency() {
        return currency;
    }

    public String getDeclinedBy() {
        return declinedBy;
    }

    public String getOrderedBy() {
        return orderedBy;
    }

    public String getPartyId() {
        return partyId;
    }

    public String getReferenceId1() {
        return referenceId1;
    }

}