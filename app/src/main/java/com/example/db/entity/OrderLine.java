package com.example.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;


@Entity(tableName = "order_line", primaryKeys = {"sequence_id", "order_id"})
public class OrderLine {

    //    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "sequence_id")
    private int sequenceId;

    @NonNull
    @ColumnInfo(name = "order_id")
    private String orderId;

    @ColumnInfo(name = "item_id")
    private String itemId;


    @ColumnInfo(name = "order_price")
    private int orderPrice;

    @ColumnInfo(name = "quantity")
    private String quantity;

    @ColumnInfo(name = "status")
    private String status;

    @ColumnInfo(name = "item_description")
    private String itemDescription;

    @ColumnInfo(name = "line_total")
    private int lineTotal;

    @ColumnInfo(name = "comments")
    private String comments;

    @ColumnInfo(name = "type")
    private String type;

    @ColumnInfo(name = "create_date")
    private String createDate;

    @ColumnInfo(name = "update_date")
    private String updateDate;

    public int getSequenceId() {
        return sequenceId;
    }

    public void setSequenceId(int sequenceId) {
        this.sequenceId = sequenceId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public int getLineTotal() {
        return lineTotal;
    }

    public void setLineTotal(int lineTotal) {
        this.lineTotal = lineTotal;
    }

    public int getOrderPrice() {
        return orderPrice;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public void setOrderPrice(int orderPrice) {
        this.orderPrice = orderPrice;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getComments() {
        return comments;
    }

    public String getCreateDate() {
        return createDate;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public String getQuantity() {
        return quantity;
    }

    public String getStatus() {
        return status;
    }

    public String getType() {
        return type;
    }

    public String getUpdateDate() {
        return updateDate;
    }
}