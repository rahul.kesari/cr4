package com.example.adapters.recyclerview;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.R;
import com.example.db.entity.OrderLine;
import com.example.utils.Constants;
import com.example.utils.customviews.CustomTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderListAdapter extends RecyclerView.Adapter {
    private final String TAG = OrderListAdapter.class.getSimpleName();
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;


    private Context mContext = null;
    private List<OrderLine> orderList;
    private StatusChangedListener mListener;

    public OrderListAdapter(Context mContext, List<OrderLine> orderList, StatusChangedListener listener) {

        this.mContext = mContext;
        this.orderList = orderList;
        this.mListener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        return orderList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.inflater_order_list, parent, false);

            vh = new OrderListViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.more_progress_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof OrderListViewHolder) {

            ((OrderListViewHolder) holder).itemDescTV.setText(orderList.get(position).getItemDescription());

            /*change card background color as per status*/
            if (orderList.get(position).getStatus().equals(Constants.STATUS_NEW)) {
                ((OrderListViewHolder) holder).foregroundLL.setBackgroundColor(ContextCompat.getColor(mContext, R.color.color_status_new));
            } else if (orderList.get(position).getStatus().equals(Constants.STATUS_DROPPED)) {
                ((OrderListViewHolder) holder).foregroundLL.setBackgroundColor(ContextCompat.getColor(mContext, R.color.color_status_dropped));
            } else if (orderList.get(position).getStatus().equals(Constants.STATUS_NA)) {
                ((OrderListViewHolder) holder).foregroundLL.setBackgroundColor(ContextCompat.getColor(mContext, R.color.color_status_na));
            } else {
                ((OrderListViewHolder) holder).foregroundLL.setBackgroundColor(ContextCompat.getColor(mContext, R.color.color_status_ordered));
            }

            /*hide show action buttons based on current status*/

            if (orderList.get(position).getStatus().equals(Constants.STATUS_DROPPED)) {
                ((OrderListViewHolder) holder).closeBtn.setVisibility(View.GONE);
            } else {
                ((OrderListViewHolder) holder).closeBtn.setVisibility(View.VISIBLE);
            }

            if (orderList.get(position).getStatus().equals(Constants.STATUS_NA)) {
                ((OrderListViewHolder) holder).downBtn.setVisibility(View.GONE);
            } else {
                ((OrderListViewHolder) holder).downBtn.setVisibility(View.VISIBLE);
            }

            if (orderList.get(position).getStatus().equals(Constants.STATUS_ORDERED)) {
                ((OrderListViewHolder) holder).upBtn.setVisibility(View.GONE);
            } else {
                ((OrderListViewHolder) holder).upBtn.setVisibility(View.VISIBLE);
            }

        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }


    @Override
    public int getItemCount() {
        return orderList.size();
    }


    public class OrderListViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.closeBtn)
        ImageView closeBtn;
        @BindView(R.id.downBtn)
        ImageView downBtn;
        @BindView(R.id.upBtn)
        ImageView upBtn;
        @BindView(R.id.view_background)
        LinearLayout viewBackground;
        @BindView(R.id.foregroundLL)
        LinearLayout foregroundLL;
        @BindView(R.id.itemDescTV)
        CustomTextView itemDescTV;

        public OrderListViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            upBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.onOrderLineAcceptedClicked(getAdapterPosition());
                    }
                }
            });

            downBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.onOrderLineRejectedClicked(getAdapterPosition());
                    }
                }
            });

            closeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mListener != null) {
                        mListener.onOrderLineClosedClicked(getAdapterPosition());
                    }
                }
            });
        }


    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBarPagination);
        }
    }

    public interface StatusChangedListener {
        void onOrderLineAcceptedClicked(int position);

        void onOrderLineRejectedClicked(int position);

        void onOrderLineClosedClicked(int position);
    }

}
