package com.example.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.R;
import com.example.baseactivities.BaseActivity;
import com.example.utils.customviews.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ErrorActivity extends BaseActivity {

    @BindView(R.id.okBtn)
    CustomTextView okBtn;

    private LayoutInflater li;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        li = getLayoutInflater();
        View view = li.inflate(R.layout.activity_error, contentFrameLayout);
        ButterKnife.bind(this, view);
        TextView toolbartitle = (TextView) findViewById(R.id.toolbar_title);
        toolbartitle.setText(getString(R.string.string_screen4));

        //Hide keyboard.
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @OnClick(R.id.okBtn)
    public void onViewClicked() {
        finish();
    }
}
