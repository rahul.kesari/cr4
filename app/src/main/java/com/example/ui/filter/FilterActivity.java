package com.example.ui.filter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.R;
import com.example.baseactivities.BaseActivity;
import com.example.utils.Constants;
import com.example.utils.customviews.CustomTextView;
import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.DatePickerDialog;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FilterActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener {

    private final String TAG = FilterActivity.class.getSimpleName();
    @BindView(R.id.alllinesCB)
    AppCompatCheckBox alllinesCB;
    @BindView(R.id.fromTV)
    CustomTextView fromTV;
    @BindView(R.id.toTV)
    CustomTextView toTV;
    @BindView(R.id.ascRB)
    AppCompatRadioButton ascRB;
    @BindView(R.id.descRB)
    AppCompatRadioButton descRB;
    @BindView(R.id.saveBtn)
    CustomTextView saveBtn;
    @BindView(R.id.cancelBtn)
    CustomTextView cancelBtn;
    @BindView(R.id.newOrdersCB)
    AppCompatCheckBox newOrdersCB;
    @BindView(R.id.orderedCB)
    AppCompatCheckBox orderedCB;
    @BindView(R.id.naCB)
    AppCompatCheckBox naCB;
    @BindView(R.id.droppedCB)
    AppCompatCheckBox droppedCB;
    @BindView(R.id.fromLL)
    LinearLayout fromLL;
    @BindView(R.id.toLL)
    LinearLayout toLL;

    private boolean isFromDateClicked = false;
    private Calendar dateSelected;
    private String fromDate = null, toDate = null;
    private LayoutInflater li;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        li = getLayoutInflater();
        View view = li.inflate(R.layout.activity_filter, contentFrameLayout);
        ButterKnife.bind(this, view);
        TextView toolbartitle = (TextView) findViewById(R.id.toolbar_title);
        toolbartitle.setText(getString(R.string.filter_items));

        //Hide keyboard.
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.saveBtn, R.id.cancelBtn, R.id.fromLL, R.id.toLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.saveBtn:
                if (validaetFields()) {
                    prepareData();
                }
                break;
            case R.id.cancelBtn:
                finish();
                break;
            case R.id.fromLL:
                isFromDateClicked = true;
                openCalendar();
                break;
            case R.id.toLL:
                isFromDateClicked = false;
                openCalendar();
                break;

        }
    }

    private void prepareData() {
        HashMap<String, String> filterHashMap = new HashMap<String, String>();

        if (ascRB.isChecked()) {
            filterHashMap.put(Constants.FILTER_SORTY_BY, Constants.ASCENDING);
        } else {
            filterHashMap.put(Constants.FILTER_SORTY_BY, Constants.DESCENDING);
        }

        if (fromDate != null)
            filterHashMap.put(Constants.FILTER_FROM_DATE, fromDate);

        if (toDate != null)
            filterHashMap.put(Constants.FILTER_TO_DATE, toDate);

        if (newOrdersCB.isChecked())
            filterHashMap.put(Constants.FILTER_STATUS_NEW, Constants.STATUS_NEW);
        if (orderedCB.isChecked())
            filterHashMap.put(Constants.FILTER_STATUS_ORDERED, Constants.STATUS_ORDERED);

        if (naCB.isChecked())
            filterHashMap.put(Constants.FILTER_STATUS_NA, Constants.STATUS_NA);

        if (droppedCB.isChecked())
            filterHashMap.put(Constants.FILTER_STATUS_DROPPED, Constants.STATUS_DROPPED);

        Intent intent = new Intent();
        intent.putExtra(Constants.FILTER_REQUEST_DATA, filterHashMap);
        setResult(RESULT_OK, intent);
        finish();
    }

    private boolean validaetFields() {
        boolean isvalid = true;

        return isvalid;
    }


    private void openCalendar() {
        dateSelected = Calendar.getInstance(TimeZone.getDefault());
        new SpinnerDatePickerDialogBuilder()
                .context(FilterActivity.this)
                .callback(this)
                .spinnerTheme(R.style.DatePickerSpinner)
                .defaultDate(2018, 0, 1)
                .build()
                .show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
        dateSelected.set(Calendar.YEAR, year);
        dateSelected.set(Calendar.MONTH, monthOfYear);
        dateSelected.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        if (isFromDateClicked) {
            fromDate = sdf.format(dateSelected.getTime());
            Log.d(TAG, "From Date onDateSet: " + fromDate);
            fromTV.setText(fromDate);

        } else {
            toDate = sdf.format(dateSelected.getTime());
            Log.d(TAG, "To Date onDateSet: " + toDate);
            toTV.setText(toDate);
        }

    }
}
