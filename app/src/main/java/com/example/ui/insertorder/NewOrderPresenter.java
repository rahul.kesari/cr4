package com.example.ui.insertorder;

import android.content.Context;
import android.os.AsyncTask;

import com.example.db.AppDatabase;
import com.example.db.entity.OrderHeader;
import com.example.db.entity.OrderLine;
import com.example.utils.Constants;
import com.example.utils.DateTimerHelper;

public class NewOrderPresenter {

    private NewOrderMvpView mvpView;
    private AppDatabase appDatabase;
    private String itemDesc;


    public NewOrderPresenter(NewOrderMvpView view, Context mcontext, AppDatabase appDatabase) {
        this.mvpView = view;
        this.appDatabase = appDatabase;

    }

    public void insertRecord(String itemDesc) {
        this.itemDesc = itemDesc;
        getMaxSequenceID task = new getMaxSequenceID();
        task.execute();

    }

    private void saveRecord(int sequenceID) {

        String currentDateTime = DateTimerHelper.getInstance().getCurrentDateTime();
        /*prepare order line*/
        OrderLine orderLine = new OrderLine();
        orderLine.setSequenceId(sequenceID + 1);
        orderLine.setOrderId(getComposedOrderId(sequenceID));
        orderLine.setStatus(Constants.STATUS_NEW);
        orderLine.setCreateDate(currentDateTime);
       // orderLine.setUpdateDate(currentDateTime);
        orderLine.setItemDescription(itemDesc);


        /*prepare order header*/
        OrderHeader orderHeader = new OrderHeader();
        orderHeader.setOrderId(getComposedOrderId(sequenceID));
        orderHeader.setStatus(Constants.STATUS_NEW);
        orderHeader.setCreateDate(currentDateTime);

        insertOrderRecord task = new insertOrderRecord(orderHeader, orderLine);
        task.execute();
    }


    private String getComposedOrderId(int sequenceID) {
        return "XX_X_" + (sequenceID + 1);
    }


    private class insertOrderRecord extends AsyncTask<Void, Void, Void> {

        private OrderHeader orderHeader;
        private OrderLine orderLine;

        private insertOrderRecord(OrderHeader orderHeader, OrderLine orderLine) {
            this.orderHeader = orderHeader;
            this.orderLine = orderLine;
        }

        @Override
        protected Void doInBackground(final Void... params) {
            appDatabase.orderLineDao().insertAll(orderLine);
            appDatabase.orderHeaderDao().insertAll(orderHeader);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mvpView.onSuccessInserted(1);
        }
    }


    private class getMaxSequenceID extends AsyncTask<Void, Void, Void> {

        int maxCount = 0;

        @Override
        protected Void doInBackground(final Void... params) {
            maxCount = appDatabase.orderLineDao().getNumberOfRows();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            saveRecord(maxCount);
        }
    }
}
