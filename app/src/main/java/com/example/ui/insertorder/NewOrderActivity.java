package com.example.ui.insertorder;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.R;
import com.example.baseactivities.BaseActivity;
import com.example.db.AppDatabase;
import com.example.db.entity.OrderHeader;
import com.example.utils.customviews.CustomEditText;
import com.example.utils.customviews.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewOrderActivity extends BaseActivity implements NewOrderMvpView {


    @BindView(R.id.saveBtn)
    CustomTextView saveBtn;
    @BindView(R.id.itemET)
    CustomEditText itemET;
    @BindView(R.id.cancelBtn)
    CustomTextView cancelBtn;

    private String TAG = NewOrderActivity.class.getSimpleName();
    private NewOrderPresenter mPresenter;

    private LayoutInflater li;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        li = getLayoutInflater();
        View view = li.inflate(R.layout.activity_new_order, contentFrameLayout);
        ButterKnife.bind(this, view);
        TextView toolbartitle = (TextView) findViewById(R.id.toolbar_title);
        toolbartitle.setText("Add Items");

        //Hide keyboard.
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        mPresenter = new NewOrderPresenter(this, this, AppDatabase.getAppDatabase(this));
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }


    @Override
    public void onSuccessInserted(int noOfrecords) {
      //  showToast("Inserted Successfully");
        finish();
    }

    @OnClick({R.id.saveBtn, R.id.cancelBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.saveBtn:
                if (itemET.getText().toString().trim().length() != 0) {
                    OrderHeader orderHeader = new OrderHeader();
                    orderHeader.setComments(itemET.getText().toString().trim());
                    mPresenter.insertRecord(itemET.getText().toString().trim());
                } else {
                    showAlertDialog("Order Cannot be null");
                }
                break;
            case R.id.cancelBtn:
                finish();
                break;

        }
    }


}
