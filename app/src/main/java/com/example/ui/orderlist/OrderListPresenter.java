package com.example.ui.orderlist;

import android.arch.persistence.db.SimpleSQLiteQuery;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.db.AppDatabase;
import com.example.db.entity.OrderHeader;
import com.example.db.entity.OrderLine;
import com.example.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OrderListPresenter {

    private OrderListMvpView mvpView;
    private AppDatabase appDatabase;
    private String TAG = OrderListPresenter.class.getSimpleName();

    public OrderListPresenter(OrderListMvpView view, Context mcontext, AppDatabase appDatabase) {
        this.mvpView = view;
        this.appDatabase = appDatabase;

    }

    public void getListofOrders(HashMap<String, String> filterParam) {
        populateOrderHeaderListTask task = new populateOrderHeaderListTask(filterParam);
        task.execute();
    }

    public void updateOrderStatus(OrderLine orderLine, OrderHeader orderHeader) {

        updateOrderStatusTask task = new updateOrderStatusTask(orderLine, orderHeader);
        task.execute();
    }


    private class populateOrderHeaderListTask extends AsyncTask<Void, Void, Void> {

        List<OrderLine> orderLineList;
        HashMap<String, String> filterMap;

        populateOrderHeaderListTask(HashMap<String, String> filterMap) {
            this.filterMap = filterMap;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mvpView.showLoading();
        }

        @Override
        protected Void doInBackground(final Void... params) {
            if (filterMap.size() > 0) {

                // List of bind parameters
                List<Object> args = new ArrayList();

//                String queryString = "select * from order_line where create_date >= ? AND create_date <= ?";
                String queryString = "SELECT * FROM order_line";
                boolean containsCondition = false;

                /*for new status*/
                if (filterMap.containsKey(Constants.FILTER_STATUS_NEW)) {

                    queryString += " WHERE status = ?";
                    args.add(filterMap.get(Constants.FILTER_STATUS_NEW));
                    containsCondition = true;
                }

                /*for ordered status*/
                if (filterMap.containsKey(Constants.FILTER_STATUS_ORDERED)) {
                    if (containsCondition) {
                        queryString += " OR status = ?";
                    } else {
                        queryString += " WHERE status = ?";
                        containsCondition = true;
                    }
                    args.add(filterMap.get(Constants.FILTER_STATUS_ORDERED));
                }


                /*for na status*/
                if (filterMap.containsKey(Constants.FILTER_STATUS_NA)) {
                    if (containsCondition) {
                        queryString += " OR status = ?";
                    } else {
                        queryString += " WHERE status = ?";
                        containsCondition = true;
                    }
                    args.add(filterMap.get(Constants.FILTER_STATUS_NA));
                }

                /*for dropped status*/
                if (filterMap.containsKey(Constants.FILTER_STATUS_DROPPED)) {
                    if (containsCondition) {
                        queryString += " OR status = ?";
                    } else {
                        queryString += " WHERE status = ?";
                        containsCondition = true;
                    }
                    args.add(filterMap.get(Constants.FILTER_STATUS_DROPPED));
                }

                /*for from date*/
                if (filterMap.containsKey(Constants.FILTER_FROM_DATE)) {

                    if (containsCondition) {
                        queryString += " AND create_date >= ?";
                    } else {
                        queryString += " WHERE create_date >= ?";
                        containsCondition = true;
                    }

                    args.add(filterMap.get(Constants.FILTER_FROM_DATE));
                }

                /*for to date*/
                if (filterMap.containsKey(Constants.FILTER_TO_DATE)) {

                    if (containsCondition) {
                        queryString += " AND create_date <= ?";
                    } else {
                        queryString += " WHERE create_date <= ?";
                        containsCondition = true;
                    }

                    args.add(filterMap.get(Constants.FILTER_TO_DATE));
                }


                /*FOR sorting ascending or descending*/
                if (filterMap.get(Constants.FILTER_SORTY_BY).equals(Constants.ASCENDING)) {
                    queryString += " ORDER BY create_date ASC";
                } else {
                    queryString += " ORDER BY create_date DESC";
                }

                Log.d(TAG, "doInBackground: Query>>> "+queryString);
                SimpleSQLiteQuery query = new SimpleSQLiteQuery(queryString, args.toArray());
                orderLineList = appDatabase.orderLineDao().getAllOrderLines(query);

            } else {
                /* without filter */
                orderLineList = appDatabase.orderLineDao().getAllOrderLines();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mvpView.onListFetched(orderLineList);
        }
    }


    private class updateOrderStatusTask extends AsyncTask<Void, Void, Void> {

        private OrderLine orderLine;
        private OrderHeader orderHeader;
        private int updatedRowCount = 0;

        public updateOrderStatusTask(OrderLine orderLine, OrderHeader orderHeader) {
            this.orderLine = orderLine;
            this.orderHeader = orderHeader;
        }

        @Override
        protected Void doInBackground(final Void... params) {

            //mvpView.authenticationFailed("some problem");


            updatedRowCount = appDatabase.orderLineDao().updateOrderLineStatus(orderLine);
            Log.d(TAG, "doInBackground: " + updatedRowCount);
            updatedRowCount = appDatabase.orderHeaderDao().updateOrderHeaderStatus(orderHeader.getStatus(), orderHeader.getComments(), orderHeader.getActionDate(), orderHeader.getOrderId());
            Log.d(TAG, "doInBackground: " + updatedRowCount);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mvpView.afterStatusUpdated(orderLine);

        }
    }


}
