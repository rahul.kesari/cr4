package com.example.ui.orderlist;


import com.example.db.entity.OrderHeader;
import com.example.db.entity.OrderLine;

import java.util.ArrayList;
import java.util.List;

public interface OrderListMvpView {

    void onListFetched(List<OrderLine> mList);

    void afterStatusUpdated(OrderLine orderLine);

    void authenticationFailed(String reason);

    void showLoading();


}
