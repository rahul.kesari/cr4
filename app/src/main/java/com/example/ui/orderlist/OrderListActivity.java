package com.example.ui.orderlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.R;
import com.example.adapters.recyclerview.OrderListAdapter;
import com.example.baseactivities.BaseActivity;
import com.example.db.AppDatabase;
import com.example.db.entity.OrderHeader;
import com.example.db.entity.OrderLine;
import com.example.dialogs.CommentDialog;
import com.example.ui.ErrorActivity;
import com.example.ui.filter.FilterActivity;
import com.example.ui.insertorder.NewOrderActivity;
import com.example.utils.Constants;
import com.example.utils.DateTimerHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderListActivity extends BaseActivity implements OrderListMvpView, OrderListAdapter.StatusChangedListener {

    @BindView(R.id.addBtn)
    FloatingActionButton addBtn;
    @BindView(R.id.orderListRV)
    RecyclerView orderListRV;
    @BindView(R.id.filterIV)
    ImageView filterIV;

    private String TAG = OrderListActivity.class.getSimpleName();
    private OrderListPresenter orderListPresenter;
    private OrderListAdapter orderListAdapter;
    private List<OrderLine> mList = new ArrayList<>();
    private int operatedPosition;
    private int REQUEST_CODE_FILTER = 101;
    private HashMap<String, String> filterList = new HashMap<String, String>();

    private LayoutInflater li;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  setContentView(R.layout.activity_order_list);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        li = getLayoutInflater();
        View view = li.inflate(R.layout.activity_order_list, contentFrameLayout);
        ButterKnife.bind(this, view);
        TextView toolbartitle = (TextView) findViewById(R.id.toolbar_title);
        toolbartitle.setText(getString(R.string.string_screen4));

        //Hide keyboard.
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        orderListPresenter = new OrderListPresenter(this, this, AppDatabase.getAppDatabase(this));
    }


    @Override
    protected void onResume() {
        super.onResume();
        getListOfOrders();
    }

    private void getListOfOrders() {

        orderListPresenter.getListofOrders(filterList);
    }

    @OnClick({R.id.addBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.addBtn:
                startActivity(new Intent(this, NewOrderActivity.class));
                break;
        }
    }

    @Override
    public void onListFetched(List<OrderLine> mList) {
        hideProgressDialog();
        this.mList = mList;

        if (mList == null) {
            // showToast("Crashed");
            return;
        }
        if (mList.size() > 0) {

        } else {
           // showToast("No Data Found");
        }

        setRecyclerData(mList);
        for (OrderLine orderLine : mList) {
            logD(TAG, "onListFetched: " + orderLine.getOrderId() + "  " + orderLine.getStatus() + "  " + orderLine.getCreateDate());
        }
    }

    @Override
    public void afterStatusUpdated(OrderLine orderLine) {
        //showToast("Status Updated");

        mList.get(operatedPosition).setStatus(orderLine.getStatus());
        mList.get(operatedPosition).setUpdateDate(orderLine.getUpdateDate());
        orderListAdapter.notifyItemChanged(operatedPosition);
    }

    @Override
    public void authenticationFailed(String reason) {

        startActivity(new Intent(this, ErrorActivity.class));
    }

    @Override
    public void showLoading() {
        showProgressDialog("Loading...");
    }

    private void setRecyclerData(List<OrderLine> mList) {

        orderListAdapter = new OrderListAdapter(this, mList, this);
        orderListRV.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        orderListRV.setAdapter(orderListAdapter);

    }

    @Override
    public void onOrderLineAcceptedClicked(int position) {

        this.operatedPosition = position;
        //Mark the status of the order as Ordered. Change the background color of the order to GREEN.
        // Save the order status in DB along with timestamp

        updateClosedRejectedStatus("", Constants.STATUS_ORDERED);
    }

    @Override
    public void onOrderLineRejectedClicked(int position) {
        this.operatedPosition = position;

        //Mark the status of the order as NA. Change the background color of the order to YELLOW.
        // Save the order status in DB. There should be popup to capture free text with OK and CANCEL button. The free text must get saved as ORDER COMMENTS.

        showPopupForComment(Constants.STATUS_NA);
    }

    @Override
    public void onOrderLineClosedClicked(int position) {
        this.operatedPosition = position;

        //Mark the status of the order as DROPPED. Change the background color of the order to RED/ORANGE.
        // Save the order status in DB along with timestamp.
        showPopupForComment(Constants.STATUS_DROPPED);
    }


    private void showPopupForComment(String status) {

        CommentDialog commentDialog = new CommentDialog(this);
        commentDialog.setonOkListener(new CommentDialog.OkListerner() {
            @Override
            public void onOkClicked(String comment) {
                updateClosedRejectedStatus(comment, status);
            }
        });
        commentDialog.show();
    }

    private void updateClosedRejectedStatus(String comment, String status) {

        String updatedTimeDate = DateTimerHelper.getInstance().getCurrentDateTime();
        OrderLine orderLine = mList.get(operatedPosition);
        orderLine.setStatus(status);
        orderLine.setUpdateDate(updatedTimeDate);
        orderLine.setComments(comment);

        OrderHeader orderHeader = new OrderHeader();
        orderHeader.setOrderId(mList.get(operatedPosition).getOrderId());
        orderHeader.setActionDate(updatedTimeDate);
        orderHeader.setStatus(status);
        orderHeader.setComments(comment);

        orderListPresenter.updateOrderStatus(orderLine, orderHeader);
    }

    @OnClick(R.id.filterIV)
    public void onViewClicked() {

        startActivityForResult(new Intent(this, FilterActivity.class), REQUEST_CODE_FILTER);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_FILTER && resultCode == RESULT_OK) {

            filterList = (HashMap<String, String>) data.getSerializableExtra(Constants.FILTER_REQUEST_DATA);
            orderListPresenter.getListofOrders(filterList);
        }
    }
}
